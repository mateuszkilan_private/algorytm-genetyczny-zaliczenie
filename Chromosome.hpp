#pragma once

#include <utility>

struct Chromosome {
    std::pair<float, float> rouletteRange;
    float roulettePercent;
    float phenotype;
    unsigned int gene0 : 1;
    unsigned int gene1 : 1;
    unsigned int gene2 : 1;
    unsigned int gene3 : 1;
    unsigned int gene4 : 1;
    unsigned int gene5 : 1;
    unsigned int gene6 : 1;
    float adaptation;
};

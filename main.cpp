#include <iostream>
#include <string>
#include <cmath>
#include <time.h>
#include <stdlib.h>
#include <bitset>

#include "Chromosome.hpp"
#include "helpers.hpp"
#include "debuggers.hpp"
#include "inputCheckers.hpp"

using namespace std;

int main() {
    ////MANUAL TEST FOR INPUT CHECKERS BELOW
    //string test = "";
    //while (true) {
    //    cout << "\nget test string: ";
    //    cin >> test;
    //    //bool isCorrect = isInputCorrectInteger(test);
    //    //bool isCorrect = isInputCorrectNatural(test);
    //    bool isCorrect = isInputCorrectNaturalFloat(test);

    //    if (isCorrect) {
    //        cout << "\ncorrect!";
    //    } else {
    //        cout << "\nNOT correct!";
    //    }
    //}
    srand(time(NULL));
    printWelcomeMessage();
    getUserInputForAllNeededData();
    // DEBUG_showAllInputDataValues();

    // chooseStartingPopulation and preparePopulationsAdaptation should be invoked only at the beginning of the algorithm
    chooseStartingPopulation();
    //DEBUG_LogPopulation();
    noOfCreatedGenerations++;
    prepareMutationSelectionTuples();
    //DEBUG_mutSelVec();
    calculatePopulationsAdaptation();

    incrementAdaptationIfNeeded();
    calculatePercentageRoulette();
    decrementAdaptationIfNeeded();
    // DEBUG_LogPopulation();

    for (unsigned int i = 1; i < noOfGenerations; i++) {
        // selection and crossover
        pickChromosomesIntoPairs(); // TODO tutaj wszystko wydaje sie byc ok
        //DEBUG_LogChromosomeVecIfNullPtr();
        //DEBUG_LogMutSelVecIfParentsNullptr();
        chooseParentPairsThatWillBeCrossOvered(); // TODO tutaj wszystko wydaje sie byc ok
        chooseCrossoverPoints(); // TODO tutaj wszystko wydaje sie byc ok
        runCrossoverAlgorithm(); // TODO tutaj wszystko wydaje sie byc ok

        // mutation
        runMutationAlgorithm(); // TODO tutaj wszystko wydaje sie byc ok

        //DEBUG_LogPopulation();
        //DEBUG_mutSelVec();

        copyOffspringsFromMutSelVecToChromosomeVec(); // TODO tutaj wszystko wydaje sie byc ok
        noOfCreatedGenerations++;
 
        calculatePopulationsPhenotype();
        calculatePopulationsAdaptation(); // TODO tutaj moze byc blad
        incrementAdaptationIfNeeded();
        calculatePercentageRoulette();
        decrementAdaptationIfNeeded();

        if (stopAlgorithmIfNoImprovementForBestSpecimenInNIterations()) { // TODO tutaj byl bug , naprawilem go
            break;
        }
    }

    DEBUG_roulette(); 
    programFinishedPrintResults();
    //DEBUG_roulette();
    //DEBUG_LogPopulation();
}


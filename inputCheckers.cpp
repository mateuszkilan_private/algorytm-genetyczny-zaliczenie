#include <iostream>
#include "inputCheckers.hpp"

using namespace std;

bool isInputCorrectInteger(std::string& input) {
    if (
        !input.size() ||
        input[0] == '0' && input.size() != 1 ||
	input.size() >= 3 && input[0] == '-' && input[1] == '0'
    ) {
        return false;
    }

    for (unsigned int i = 0; i < input.size(); i++) {
        switch (input[i]) {
            case '0':
	    case '1':
	    case '2':
	    case '3':
	    case '4':
	    case '5':
	    case '6':
	    case '7':
	    case '8':
	    case '9':
                // we do nothing really. It is ok :)
	        break;
            default:
		if (i == 0 && input[i] == '-') break; // it's ok first char can be minus
	        // it means that there is illegal character
                return false;
	}
    }

    return true;
}

bool isInputCorrectNatural(std::string& input) {
    if (!input.size() || input[0] == '0' && input.size() != 1) {
        return false;
    }

    for (unsigned int i = 0; i < input.size(); i++) {
        switch (input[i]) {
            case '0':
	    case '1':
	    case '2':
	    case '3':
	    case '4':
	    case '5':
	    case '6':
	    case '7':
	    case '8':
	    case '9':
                // we do nothing really. It is ok :)
	        break;
            default:
	        // it means that there is illegal character
                return false;
	}
    }

    return true;
}

bool isInputCorrectNaturalFloat(std::string& input) {
    if (!input.size() || input.size() >= 2 && input[0] == '0' && input[1] != '.' || input[0] == '.') {
        return false;
    }

    bool isThereAlreadyDot = false;

    for (unsigned int i = 0; i < input.size(); i++) {
        switch (input[i]) {
	    case '.':
               {
	           if (isThereAlreadyDot == false) {
		       isThereAlreadyDot = true;
		   } else {
		       return false;
		   }
		   break;
	       }
            case '0':
	    case '1':
	    case '2':
	    case '3':
	    case '4':
	    case '5':
	    case '6':
	    case '7':
	    case '8':
	    case '9':
                // we do nothing really. It is ok :)
	        break;
            default:
	        // it means that there is illegal character
                return false;
	}
    }

    return true;
}

void printWelcomeMessage() {
    cout << "Welcome to genetic algorithm. This algorithm's purpose is to optimize mathematical function\n"
         << "Adaptation function's euqation is equal to = paramA * sqrt(x) + paramB * log10(x) + cos(pow(x, 2))\n"
         << "First we need to set \"a\" and \"b\" parameters of the function\n";
}

static void _getUserInputForParamA();
static void _getUserInputForParamB();
static void _getUserInputForPopulationSize();
static void _getUserInputForCrossoverPointsNo();
static void _getUserInputForCrossoverProbability();
static void _getUserInputForMutationProbability();
static void _getUserInputForNoOfGenerations();
static void _getUserInputForStopAfterNIterationsWithoutImprovement();


void getUserInputForAllNeededData() {
    _getUserInputForParamA();
    _getUserInputForParamB();
    _getUserInputForPopulationSize();
    _getUserInputForCrossoverPointsNo();
    _getUserInputForCrossoverProbability();
    _getUserInputForMutationProbability();
    _getUserInputForNoOfGenerations();
    _getUserInputForStopAfterNIterationsWithoutImprovement();
}


static void _getUserInputForParamA() {
    // paramA
    cout << "\nParamater \"a\" = ";
    cin >> inputFromUser;
    while (true) {
        if (!isInputCorrectInteger(inputFromUser)) {
            cout << "\nInvalid integer number, please try again. Parameter \"a\" = ";
            cin >> inputFromUser;
            continue;
        }

        paramA = std::stoi(inputFromUser);
        break;
    }
    // END paramA
}
static void _getUserInputForParamB() {
    // paramB
    cout << "\nParamater \"b\" = ";
    cin >> inputFromUser;
    while (!isInputCorrectInteger(inputFromUser)) {
        cout << "\nInvalid integer number, please try again. Parameter \"b\" = ";
        cin >> inputFromUser;
    }
    paramB = std::stoi(inputFromUser);
    // END paramB
}
static void _getUserInputForPopulationSize() {
    // populationSize
    cout << "\nNow, please choose population size, use even number: ";
    cin >> inputFromUser;
    while (true) {
        bool isCorrectNatural = isInputCorrectNatural(inputFromUser);

        if (!isCorrectNatural || isCorrectNatural && std::stoi(inputFromUser) % 2 != 0 || isCorrectNatural && std::stoi(inputFromUser) == 0) {
            cout << "\nInvalid natural, even number, please try again. Population size = ";
            cin >> inputFromUser;
            continue;
        }

        populationSize = std::stoi(inputFromUser);
        break;
    }
    // END populationSize
}
static void _getUserInputForCrossoverPointsNo() {
    // crossoverPointsNo
    cout << "\nChoose number of crossover points from 1 to 6: ";
    cin >> inputFromUser;
    while (true) {
        bool isCorrectNatural = isInputCorrectNatural(inputFromUser);

        if (!isCorrectNatural || isCorrectNatural && (std::stoi(inputFromUser) < 1 || isCorrectNatural && std::stoi(inputFromUser) > 6)) {
            cout << "\nInvalid natural number between 1 and 6, please try again. Crossover points number = ";
            cin >> inputFromUser;
            continue;
        }

        crossoverPointsNo = std::stoi(inputFromUser);
        break;
    }
    // END crossoverPointsNo
}
static void _getUserInputForCrossoverProbability() {
    // crossoverProbability
    cout << "\nChoose crossover probability - float between 0 and 1: ";
    cin >> inputFromUser;
    while (true) {
        bool isCorrectNaturalFloat = isInputCorrectNaturalFloat(inputFromUser);

        if (!isCorrectNaturalFloat || isCorrectNaturalFloat && std::stof(inputFromUser) > 1.0f) {
            cout << "\nInvalid natural float number. Probability is between 0 and 1, please try again. Crossover probability = ";
            cin >> inputFromUser;
            continue;
        }

        crossoverProbability = std::stof(inputFromUser);
        break;
    }
    // END crossoverProbability
}
static void _getUserInputForMutationProbability() {
    // mutationProbability
    cout << "\nChoose mutation probability - float between 0 and 1: ";
    cin >> inputFromUser;
    while (true) {
        bool isCorrectNaturalFloat = isInputCorrectNaturalFloat(inputFromUser);

        if (!isCorrectNaturalFloat || isCorrectNaturalFloat && std::stof(inputFromUser) > 1.0f) {
            cout << "\nInvalid natural float number. Probability is between 0 and 1, please try again. Mutation probability = ";
            cin >> inputFromUser;
            continue;
        }

        mutationProbability = std::stof(inputFromUser);
        break;
    }
    // END mutationProbability
}
static void _getUserInputForNoOfGenerations() {
    // noOfGenerations
    cout << "\nType in number of generations to produce: ";
    cin >> inputFromUser;
    while (true) {
        bool isCorrectNatural = isInputCorrectNatural(inputFromUser);

        if (!isCorrectNatural || isCorrectNatural && std::stoi(inputFromUser) == 0) {
            cout << "\nInvalid natural number greater than 0, please try again. Number of generations: ";
            cin >> inputFromUser;
            continue;
        }

        noOfGenerations = std::stoi(inputFromUser);
        break;
    }
    // END noOfGenerations
}
static void _getUserInputForStopAfterNIterationsWithoutImprovement() {
    // stopAfterNIterationsWithoutImprovement
    cout << "\nAfter how many iterations should algorithm be stopped if there was no improvement for the best specimen? ";
    cin >> inputFromUser;
    while (true) {
        bool isCorrectNatural = isInputCorrectNatural(inputFromUser);

        if (!isCorrectNatural || isCorrectNatural && std::stoi(inputFromUser) < 2) {
            cout << "\nInvalid natural number greater or equal to 2, please try again. Stop algorithm after how many iterations without improvement? ";
            cin >> inputFromUser;
            continue;
        }

        stopAfterNIterationsWithoutImprovement = std::stoi(inputFromUser);
        break;
    }
    // END stopAfterNIterationsWithoutImprovement
}


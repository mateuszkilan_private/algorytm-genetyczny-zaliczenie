#include "debuggers.hpp"
#include "helpers.hpp"
#include <iostream>

using namespace std;

void DEBUG_LogMutSelVecIfParentsNullptr() {
    cout << "\n\n ===== START DEBUG_LogMutSelVecIfParentsNullptr\n\n";
    for (unsigned int i = 0; i < mutSelVec.size(); i++) {
        if (get<PARENTS_PTRS_PAIR>(mutSelVec[i]).first == nullptr || get<PARENTS_PTRS_PAIR>(mutSelVec[i]).first == NULL) {
            cout << "\nget<PARENTS_PTRS_PAIR>(mutSelVec[" << i << "]).first is nullptr/NULL";
        }
        if (get<PARENTS_PTRS_PAIR>(mutSelVec[i]).second == nullptr || get<PARENTS_PTRS_PAIR>(mutSelVec[i]).second == NULL) {
            cout << "\nget<PARENTS_PTRS_PAIR>(mutSelVec[" << i << "]).second is nullptr/NULL";
        }
    }
    cout << "\n\n ===== END DEBUG_LogMutSelVecIfParentsNullptr";
}

void DEBUG_LogChromosomeVecIfNullPtr() {
    cout << "\n\n ===== START DEBUG_LogChromosomeVecIfNullPtr";
    for (unsigned int i = 0; i < chromosomeVec.size(); i++) {
        if (&chromosomeVec[i] == nullptr || &chromosomeVec[i] == NULL) {
            cout << "\n chromosomeVec[" << i << "] is nullptr/NULL";
        }
    }
    cout << "\n\n ===== END DEBUG_LogChromosomeVecIfNullPtr";
}

void DEBUG_LogPopulation() {
    cout << "\npopulationAdaptation: " << populationAdaptation << '\n';

    for (unsigned int i = 0; i < chromosomeVec.size(); i++) {
        cout << "\n" << "chromosome[" << i << "] = {"
            << chromosomeVec[i].gene6 << " "
            << chromosomeVec[i].gene5 << " "
            << chromosomeVec[i].gene4 << " "
            << chromosomeVec[i].gene3 << " "
            << chromosomeVec[i].gene2 << " "
            << chromosomeVec[i].gene1 << " "
            << chromosomeVec[i].gene0 << " "
            << "} phenotype: " << chromosomeVec[i].phenotype
            << " adaptation: " << chromosomeVec[i].adaptation
            << " roulettePercent: " << chromosomeVec[i].roulettePercent
            << " rouletteRange.first = " << chromosomeVec[i].rouletteRange.first
            << " rouletteRange.second = " << chromosomeVec[i].rouletteRange.second
            << " &chromosomeVec[" << i << "] = " << &chromosomeVec[i]; 
    }
}

void DEBUG_mutSelVec() {
    cout << "\n\nDEBUG_mutSelVec ============================\n";

    for (unsigned int i = 0; i < mutSelVec.size(); i++) {
        cout << "\n``````````````````````````````````````````";
        mutationSelectionTuple& mutSelTuple = mutSelVec[i];

        cout << "\nmutSelVec[" << i << "]<PARENTS_PTRS_PAIR>.first = " << get<PARENTS_PTRS_PAIR>(mutSelTuple).first;
        cout << "\nmutSelVec[" << i << "]<PARENTS_PTRS_PAIR>.second = " << get<PARENTS_PTRS_PAIR>(mutSelTuple).second;
        cout << "\nmutSelVec[" << i << "]<WILL_BE_CROSSOVERED> = " << get<WILL_BE_CROSSOVERED>(mutSelTuple);

        vector<int>& crossoverPoints = get<CROSSOVER_POINTS>(mutSelTuple);

        for (unsigned int j = 0; j < crossoverPoints.size(); j++) {
            cout << "\nmutSelVec[" << i << "]<CROSSOVER_POINTS>[" << j << "] = " << crossoverPoints[j]; 
        }

        Chromosome& offspringOne = get<OFFSPRING_PAIR>(mutSelTuple).first;
        Chromosome& offspringTwo = get<OFFSPRING_PAIR>(mutSelTuple).second;
        Chromosome& parentOne = *(get<PARENTS_PTRS_PAIR>(mutSelTuple).first);
        Chromosome& parentTwo = *(get<PARENTS_PTRS_PAIR>(mutSelTuple).second);

        cout << "\nmutSelVec[" << i << "]<PARENTS_PTRS_PAIR>.first = ";
        cout << "  {" << parentOne.gene6 << " "
            << parentOne.gene5 << " "
            << parentOne.gene4 << " "
            << parentOne.gene3 << " "
            << parentOne.gene2 << " "
            << parentOne.gene1 << " "
            << parentOne.gene0 << " }";

        cout << "\nmutSelVec[" << i << "]<PARENTS_PTRS_PAIR>.second = ";
        cout << " {" << parentTwo.gene6 << " "
            << parentTwo.gene5 << " "
            << parentTwo.gene4 << " "
            << parentTwo.gene3 << " "
            << parentTwo.gene2 << " "
            << parentTwo.gene1 << " "
            << parentTwo.gene0 << " }";

        cout << "\nmutSelVec[" << i << "]<OFFSPRING_PAIR>.first = ";
        cout << "     {" << offspringOne.gene6 << " "
            << offspringOne.gene5 << " "
            << offspringOne.gene4 << " "
            << offspringOne.gene3 << " "
            << offspringOne.gene2 << " "
            << offspringOne.gene1 << " "
            << offspringOne.gene0 << " }";

        cout << "\nmutSelVec[" << i << "]<OFFSPRING_PAIR>.second = ";
        cout << "    {" << offspringTwo.gene6 << " "
            << offspringTwo.gene5 << " "
            << offspringTwo.gene4 << " "
            << offspringTwo.gene3 << " "
            << offspringTwo.gene2 << " "
            << offspringTwo.gene1 << " "
            << offspringTwo.gene0 << " }";

        cout << "\nmutSelVec[" << i << "]<WILL_OFFSPRINGS_BE_MUTATED_PAIR>.first = " << get<WILL_OFFSPRINGS_BE_MUTATED_PAIR>(mutSelTuple).first;
        cout << "\nmutSelVec[" << i << "]<WILL_OFFSPRINGS_BE_MUTATED_PAIR>.second = " << get<WILL_OFFSPRINGS_BE_MUTATED_PAIR>(mutSelTuple).second;

        cout << "\nmutSelVec[" << i << "]<MUTATION_GENES_PAIR>.first = " << get<MUTATION_GENES_PAIR>(mutSelTuple).first;
        cout << "\nmutSelVec[" << i << "]<MUTATION_GENES_PAIR>.second = " << get<MUTATION_GENES_PAIR>(mutSelTuple).second;

        cout << "\n``````````````````````````````````````````";
    }

    cout << "\n\nEND ================= DEBUG_mutSelVec ============================\n";
}

void DEBUG_roulette() {
    cout << "\nDEBUG_roulette ============================";

    for (unsigned int i = 0; i < populationSize; i++) {
        cout << "\nrouletteSpinNumbers[" << i << "] = " << rouletteSpinNumbers[i]; 
    }
}

void DEBUG_showAllInputDataValues() {
    cout << "\nparamA = " << paramA
        << "\nparamB = " << paramB
        << "\npopulationSize = " << populationSize
        << "\ncrossoverPointsNo = " << crossoverPointsNo
        << "\ncrossoverProbability = " << crossoverProbability
        << "\nmutationProbability = " << mutationProbability
    << "\nnoOfGenerations = " << noOfGenerations
    << "\nstopAfterNIterationsWithoutImprovement = " << stopAfterNIterationsWithoutImprovement << "\n\n";
}

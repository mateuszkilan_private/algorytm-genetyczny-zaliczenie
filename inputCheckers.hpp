#pragma once
#include <string>
#include "helpers.hpp"

bool isInputCorrectInteger(std::string& input);
bool isInputCorrectNatural(std::string& input);
bool isInputCorrectNaturalFloat(std::string& input);

void printWelcomeMessage();
void getUserInputForAllNeededData();

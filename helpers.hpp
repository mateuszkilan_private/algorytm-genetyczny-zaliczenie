#ifndef HELPERS_HPP
#define HELPERS_HPP

#include "Chromosome.hpp"
#include <vector>
#include <cmath>
#include <time.h>
#include <stdlib.h>
#include <tuple>
#include <utility>
#include <map>

enum MutSelAccess {
    PARENTS_PTRS_PAIR = 0,
    WILL_BE_CROSSOVERED = 1,
    CROSSOVER_POINTS = 2,
    OFFSPRING_PAIR = 3,
    WILL_OFFSPRINGS_BE_MUTATED_PAIR = 4,
    MUTATION_GENES_PAIR = 5
};

enum NextMutSelVecParentsPair {
    CURRENT_PARENTS_PAIR_PTR = 0,
    MUTSELVEC_IDX = 1,
    CURRENT_PARENT_IN_PAIR = 2
};

using parentsPairPtr = std::pair<Chromosome*, Chromosome*>*;
using chIt = std::vector<Chromosome>::iterator;
using mutationSelectionTuple = std::tuple<
    std::pair<Chromosome*, Chromosome*>, // 2 pointers onto pair selected parents
    bool, // will this pair be crossed?
    std::vector<int>, // crossover points
    std::pair<Chromosome, Chromosome>, // 2 offsprings Chromosome
    std::pair<bool, bool>, // Will first or second offspring be mutated?
    std::pair<int, int> // mutation genes for offspring one and two
>;
using currentParentsPairTuple = std::tuple<
    std::pair<Chromosome*, Chromosome*>*,
    unsigned int,
    unsigned short int
>;

extern float isNegativeAdaptation;
extern float lowestAdaptationIncremented;
extern std::string inputFromUser;
extern float previousBestSpecimenAdaptation;
extern unsigned int stopAfterNIterationsWithoutImprovement;
extern unsigned int iterationsWithoutImprovement;
extern unsigned int noOfGenerations;
extern unsigned int noOfCreatedGenerations;
extern std::map<int, int> crossoverPointsToGenesMap;
extern std::vector<mutationSelectionTuple*> mutSelTuplesForCrossover;
extern short int crossoverPointsNo;
extern int paramA;
extern int paramB;
extern unsigned int populationSize;
extern float populationAdaptation;
extern std::vector<Chromosome> chromosomeVec;
extern std::vector<mutationSelectionTuple> mutSelVec;
extern currentParentsPairTuple currentParentsTuple;
extern std::vector<int> rouletteSpinNumbers;
extern float mutationProbability;
extern float crossoverProbability;

float _calculateAdaptationForSpecimen(Chromosome& specimen);
void calculatePopulationsAdaptation();
void decrementAdaptationIfNeeded();
void incrementAdaptationIfNeeded();
void chooseStartingPopulation();
void calculatePercentageRoulette();
void pickChromosomesIntoPairs();
void prepareMutationSelectionTuples();
void chooseParentPairsThatWillBeCrossOvered();
void chooseCrossoverPoints();
void runCrossoverAlgorithm();
void runMutationAlgorithm();
void _calculatePhenotypeForSpecimen(Chromosome& specimen);
void calculatePopulationsPhenotype();
void copyOffspringsFromMutSelVecToChromosomeVec();
bool stopAlgorithmIfNoImprovementForBestSpecimenInNIterations();
void programFinishedPrintResults();

#endif


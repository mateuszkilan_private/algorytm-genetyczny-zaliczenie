#include "helpers.hpp"
#include <iostream>
#include <algorithm>
#include <bitset>
#include <cstring>

using namespace std;

float isNegativeAdaptation = 0.0f;
float lowestAdaptationIncremented = 0.0f; 
std::string inputFromUser = "";
unsigned int noOfCreatedGenerations = 0;
float previousBestSpecimenAdaptation = -1.0f; // we want to start algorithm with -1.0 because we don't have best specimen after generating initial population
unsigned int stopAfterNIterationsWithoutImprovement = 0;
unsigned int iterationsWithoutImprovement = 0;
unsigned int noOfGenerations = 0;
int paramA = 0;
int paramB = 0;
unsigned int populationSize = 0;
float populationAdaptation = 0;
vector<Chromosome> chromosomeVec = {};
short int crossoverPointsNo = 0;
vector<mutationSelectionTuple> mutSelVec = {};
currentParentsPairTuple currentParentsTuple;
vector<int> rouletteSpinNumbers = {};
float mutationProbability = 0.0f;
float crossoverProbability = 0.0f;
vector<mutationSelectionTuple*> mutSelTuplesForCrossover = {};
std::map<int, int> crossoverPointsToGenesMap = {
    {0, 6},
    {1, 5},
    {2, 4},
    {3, 3},
    {4, 2},
    {5, 1},
    {6, 0}
};

static void _setCurrentParentsTuple(unsigned int newIdx) {
    if (newIdx == mutSelVec.size()) {
        //cout << "\n\nXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX newIdx = " << newIdx << "\n";
        return;
    }

    get<CURRENT_PARENTS_PAIR_PTR>(currentParentsTuple) = &get<PARENTS_PTRS_PAIR>(mutSelVec[newIdx]);
    get<MUTSELVEC_IDX>(currentParentsTuple) = newIdx;
    get<CURRENT_PARENT_IN_PAIR>(currentParentsTuple) = 0;
}

static void _setCurrentParentToSecond() {
    get<CURRENT_PARENT_IN_PAIR>(currentParentsTuple) = 1;
}

float _calculateAdaptationForSpecimen(Chromosome& specimen) {
    // if (specimen.phenotype == 0) {
    // 	// without this, ugly bug will appear in the case when 0 phenotype specimen will be "born". It causes whole of algorithm to fail.
    //     return 0;
    // }

    //return paramA * sqrt(specimen.phenotype) + paramB * log10(specimen.phenotype) + cos(pow(specimen.phenotype, 2));
    return paramA * sqrt(specimen.phenotype) + paramB * sin(specimen.phenotype) + cos(pow(specimen.phenotype, 2));
}

void calculatePopulationsPhenotype() {
    for (unsigned int i = 0; i < populationSize; i++) {
        _calculatePhenotypeForSpecimen(chromosomeVec[i]); 
    }
}

void _calculatePhenotypeForSpecimen(Chromosome& specimen) {
    specimen.phenotype = specimen.gene6 * pow(2, 6) +
        specimen.gene5 * pow(2, 5) +
        specimen.gene4 * pow(2, 4) +
        specimen.gene3 * pow(2, 3) +
        specimen.gene2 * pow(2, 2) +
        specimen.gene1 * pow(2, 1) +
        specimen.gene0 * pow(2, 0);
}

void chooseStartingPopulation() {
    chromosomeVec.resize(populationSize); 

    for (chIt it = chromosomeVec.begin(); it != chromosomeVec.end(); it++) {
        Chromosome& specimen = *it;

        specimen.gene6 = rand() % 2;
        specimen.gene5 = rand() % 2;
        specimen.gene4 = rand() % 2;
        specimen.gene3 = rand() % 2;
        specimen.gene2 = rand() % 2;
        specimen.gene1 = rand() % 2;
        specimen.gene0 = rand() % 2;

        _calculatePhenotypeForSpecimen(specimen); 
    }
}

void calculatePopulationsAdaptation() {
    populationAdaptation = 0.0f;

    for (unsigned int i = 0; i < chromosomeVec.size(); i++) {
        chromosomeVec[i].adaptation = _calculateAdaptationForSpecimen(chromosomeVec[i]);
        populationAdaptation += chromosomeVec[i].adaptation;
    }
}

void incrementAdaptationIfNeeded() {
    isNegativeAdaptation = false;
    float lowestAdaptation = chromosomeVec[0].adaptation;

    for (unsigned int i = 0; i < chromosomeVec.size(); i++) {
        if (chromosomeVec[i].adaptation <= 0.0) {
            isNegativeAdaptation = true;
        }

        lowestAdaptation = chromosomeVec[i].adaptation < lowestAdaptation ? chromosomeVec[i].adaptation : lowestAdaptation;
    }

    if (!isNegativeAdaptation) {
        return;
    }

    lowestAdaptationIncremented = std::abs(lowestAdaptation) + 1.0;
    populationAdaptation = 0.0f; 

    for (unsigned int i = 0; i < chromosomeVec.size(); i++) {
        chromosomeVec[i].adaptation += lowestAdaptationIncremented;
        populationAdaptation += chromosomeVec[i].adaptation;
    }
}

void decrementAdaptationIfNeeded() {
    if (!isNegativeAdaptation) {
        return; 
    }

    populationAdaptation = 0.0f;

    for (unsigned int i = 0; i < chromosomeVec.size(); i++) {
        chromosomeVec[i].adaptation -= lowestAdaptationIncremented;
        populationAdaptation += chromosomeVec[i].adaptation;
    }
}

void calculatePercentageRoulette() {
    for (unsigned int i = 0; i < chromosomeVec.size(); i++) {
        chromosomeVec[i].roulettePercent = (chromosomeVec[i].adaptation / populationAdaptation) * 100;
    }
}

static bool _sortingRouletteFunc(Chromosome& chA, Chromosome& chB) {
    return chA.roulettePercent < chB.roulettePercent;
}

static bool _sortCrossoverPoints(int& pA, int& pB) {
    return pA < pB;
}

void pickChromosomesIntoPairs() {
    _setCurrentParentsTuple(0); // clean up before working with currentParentsTuple
    sort(chromosomeVec.begin(), chromosomeVec.end(), _sortingRouletteFunc);
    // get<CURRENT_PARENTS_PAIR_PTR>(currentParentsTuple) - currentParentsTuple w tym miejscu zawiera wskaznik na pair parents w mutSelVec
    chromosomeVec[0].rouletteRange.first = 0.0f;
    chromosomeVec[0].rouletteRange.second = chromosomeVec[0].roulettePercent;

    for (unsigned int i = 1; i < chromosomeVec.size(); i++) {
        chromosomeVec[i].rouletteRange.first = chromosomeVec[i - 1].rouletteRange.second;
        chromosomeVec[i].rouletteRange.second = chromosomeVec[i].rouletteRange.first + chromosomeVec[i].roulettePercent;
    }

    rouletteSpinNumbers.resize(chromosomeVec.size());

    // TODO Do tego momentu jest ok
    for (unsigned int i = 0; i < rouletteSpinNumbers.size(); i++) {
        rouletteSpinNumbers[i] = rand() % 101;

        if (rouletteSpinNumbers[i] >= chromosomeVec[0].rouletteRange.first && rouletteSpinNumbers[i] <= chromosomeVec[0].rouletteRange.second) {
            parentsPairPtr currentParents = get<CURRENT_PARENTS_PAIR_PTR>(currentParentsTuple);
 
            if (get<CURRENT_PARENT_IN_PAIR>(currentParentsTuple) == 0) {
                //cout << "\n===== CHANGING PARENT TO SECOND =====";
                //cout << "\ncurrentParentsTuple MUTSELVEC_IDX = " << get<MUTSELVEC_IDX>(currentParentsTuple);

                (*currentParents).first = &chromosomeVec[0];
                _setCurrentParentToSecond(); 
            } else {
                //cout << "\n===== CHANGING MUTSELVEC =====";
                //cout << "\ncurrentParentsTuple MUTSELVEC_IDX = " << get<MUTSELVEC_IDX>(currentParentsTuple)
                //    << "\nincremented currentParentsTuple MUTSELVEC_IDX = " << get<MUTSELVEC_IDX>(currentParentsTuple) + 1;

                (*currentParents).second = &chromosomeVec[0];
                _setCurrentParentsTuple(get<MUTSELVEC_IDX>(currentParentsTuple) + 1); 
            }
            continue;
        }
 
        for (unsigned int j = 0; j < populationSize; j++) {
            //cout << "\nj = " << j << "\n";
            if (rouletteSpinNumbers[i] >= chromosomeVec[j].rouletteRange.first && rouletteSpinNumbers[i] <= chromosomeVec[j].rouletteRange.second) {
                parentsPairPtr currentParents = get<CURRENT_PARENTS_PAIR_PTR>(currentParentsTuple);

                if (get<CURRENT_PARENT_IN_PAIR>(currentParentsTuple) == 0) {
                    //cout << "\n===== CHANGING PARENT TO SECOND =====";
                    //cout << "\ncurrentParentsTuple MUTSELVEC_IDX = " << get<MUTSELVEC_IDX>(currentParentsTuple);

                    (*currentParents).first = &chromosomeVec[j];
                    _setCurrentParentToSecond();
                } else {
                    //cout << "\n===== CHANGING MUTSELVEC =====";
                    //cout << "\ncurrentParentsTuple MUTSELVEC_IDX = " << get<MUTSELVEC_IDX>(currentParentsTuple)
                    //    << "\nincremented currentParentsTuple MUTSELVEC_IDX = " << get<MUTSELVEC_IDX>(currentParentsTuple) + 1;

                    (*currentParents).second = &chromosomeVec[j];
                    _setCurrentParentsTuple(get<MUTSELVEC_IDX>(currentParentsTuple) + 1);
                }
                break;
            }
        }
    }
}

void prepareMutationSelectionTuples() {
    mutSelVec.resize(populationSize / 2);

    for (unsigned int i = 0; i < mutSelVec.size(); i++) {
        mutationSelectionTuple& mutSelTuple = mutSelVec[i];

        get<PARENTS_PTRS_PAIR>(mutSelTuple).first = nullptr; 
        get<PARENTS_PTRS_PAIR>(mutSelTuple).second = nullptr;

        get<WILL_BE_CROSSOVERED>(mutSelTuple) = false; // we don't know yet

        get<CROSSOVER_POINTS>(mutSelTuple).resize(0);

        get<OFFSPRING_PAIR>(mutSelTuple).first = Chromosome{};
        get<OFFSPRING_PAIR>(mutSelTuple).second = Chromosome{};

        get<WILL_OFFSPRINGS_BE_MUTATED_PAIR>(mutSelTuple).first = false;  // we don't know yet
        get<WILL_OFFSPRINGS_BE_MUTATED_PAIR>(mutSelTuple).second = false; // we don't know yet

        get<MUTATION_GENES_PAIR>(mutSelTuple).first = -1;
        get<MUTATION_GENES_PAIR>(mutSelTuple).second = -1;
    }
}

void chooseParentPairsThatWillBeCrossOvered() {
    mutSelTuplesForCrossover.resize(0);

    for (unsigned int i = 0; i < mutSelVec.size(); i++) {
        //cout << "\n ========================= chooseParentPairsThatWillBeCrossovered ====================";
        //cout << "\n static_cast<float>(rand() % 101) = " << static_cast<float>(rand() % 101);
        //cout << "\n static_cast<float>(rand() % 101) / 100.0f = " << static_cast<float>(rand() % 101) / 100.0f;
        //cout << "\n =====================================================================================";
        if (static_cast<float>(rand() % 101) / 100.0f <= crossoverProbability) {
            get<WILL_BE_CROSSOVERED>(mutSelVec[i]) = true;
            //mutSelTuplesForCrossover.resize(mutSelTupleForCrossover.size() + 1);
            mutSelTuplesForCrossover.emplace_back(&mutSelVec[i]);
            continue; 
        }

        get<WILL_BE_CROSSOVERED>(mutSelVec[i]) = false; 
    } 
}

void chooseCrossoverPoints() {
    for (unsigned int i = 0; i < mutSelTuplesForCrossover.size(); i++) {
        get<CROSSOVER_POINTS>(*mutSelTuplesForCrossover[i]).resize(0); // clean up
        vector<int> randomPoints = {};

        for (short int j = 0; j < crossoverPointsNo; j++) {
            while (true) {
                int randCrossoverPoint = rand() % 6 + 1;

                if (find(randomPoints.begin(), randomPoints.end(), randCrossoverPoint) == randomPoints.end()) {
                    get<CROSSOVER_POINTS>(*mutSelTuplesForCrossover[i]).emplace_back(randCrossoverPoint);
                    randomPoints.emplace_back(randCrossoverPoint);
                    break; 
                }
            } 
        } 
    }

    for (unsigned int i = 0; i < mutSelTuplesForCrossover.size(); i++) {
        sort(get<CROSSOVER_POINTS>(*mutSelTuplesForCrossover[i]).begin(), get<CROSSOVER_POINTS>(*mutSelTuplesForCrossover[i]).end(), _sortCrossoverPoints);
    }
}

static std::bitset<7> _getParentBitset(Chromosome& parent) {
    bitset<7> parentBitset;
    parentBitset[0] = parent.gene0;
    parentBitset[1] = parent.gene1;
    parentBitset[2] = parent.gene2;
    parentBitset[3] = parent.gene3;
    parentBitset[4] = parent.gene4;
    parentBitset[5] = parent.gene5;
    parentBitset[6] = parent.gene6;

    return parentBitset;
}

void _calculateAndSetOffspringGenes(
    vector<int>& crossoverPoints,
    bitset<7>& parentOne,
    bitset<7>& parentTwo,
    bitset<7>& offspring
) {
    bool isCurrentlyParentOne = true;
    int previousPointNo = 0;
    int currentPointNo = 0;
 
    // Operations on the left side of the crossover points
    for (unsigned int j = 0; j < crossoverPoints.size(); j++) {
       currentPointNo = crossoverPoints[j];

       for (short int k = previousPointNo; k < currentPointNo; k++) {
          int noOfTheGeneToCopy = (*(crossoverPointsToGenesMap.find(k))).second; // err here with find is not possible due to mapping limits, so we can dereference it this way
          offspring[noOfTheGeneToCopy] = isCurrentlyParentOne ? parentOne[noOfTheGeneToCopy] : parentTwo[noOfTheGeneToCopy];
       }

       isCurrentlyParentOne = !isCurrentlyParentOne; 
       previousPointNo = currentPointNo;
    }

    // Operations on the right side of the last crossover point
    for (short int k = previousPointNo; k < 7; k++) {
       int noOfTheGeneToCopy = (*(crossoverPointsToGenesMap.find(k))).second; // err here with find is not possible due to mapping limits, so we can dereference it this way
       offspring[noOfTheGeneToCopy] = isCurrentlyParentOne ? parentOne[noOfTheGeneToCopy] : parentTwo[noOfTheGeneToCopy];
    }
}

static void _copyGenesFromBitsetToOffspring(bitset<7>& offspringBitset, Chromosome& offspringInst) {
    offspringInst.gene0 = offspringBitset[0]; 
    offspringInst.gene1 = offspringBitset[1]; 
    offspringInst.gene2 = offspringBitset[2]; 
    offspringInst.gene3 = offspringBitset[3]; 
    offspringInst.gene4 = offspringBitset[4]; 
    offspringInst.gene5 = offspringBitset[5]; 
    offspringInst.gene6 = offspringBitset[6]; 
}

void runCrossoverAlgorithm() {
    for (unsigned int i = 0; i < mutSelVec.size(); i++) {
        mutationSelectionTuple& mutSelTuple = mutSelVec[i];
        std::pair<Chromosome, Chromosome>& offspringsPair = get<OFFSPRING_PAIR>(mutSelTuple);

        // if pair will NOT be crossovered then:
        if (get<WILL_BE_CROSSOVERED>(mutSelTuple) == false) {
            //cout << "\nJUST COPY!!!!!!!\n";
            std::pair<Chromosome*, Chromosome*>& parentsPair = get<PARENTS_PTRS_PAIR>(mutSelTuple);
            //if (parentsPair.first == nullptr) {
            //    cout << "\n\n=============================== we got nullptr first";
            //}
            //if (parentsPair.second == nullptr) {
            //    cout << "\n=============================== we got nullptr second";
            //}
            //if (parentsPair.second == nullptr || parentsPair.first == nullptr) {
            //    cout << "\n=============================== idx = " << i << "\n\n";
            //}
            offspringsPair.first = *(parentsPair.first);
            offspringsPair.second = *(parentsPair.second);
            continue;
        }

        // if pair will be crossovered then:
        Chromosome& parentOneInst = *(get<PARENTS_PTRS_PAIR>(mutSelTuple).first);
        Chromosome& parentTwoInst = *(get<PARENTS_PTRS_PAIR>(mutSelTuple).second);
        vector<int> crossoverPoints = get<CROSSOVER_POINTS>(mutSelTuple);

        bitset<7> parentOne = _getParentBitset(parentOneInst);
        bitset<7> parentTwo = _getParentBitset(parentTwoInst);
        bitset<7> offspringOneBitset;
        bitset<7> offspringTwoBitset;

        _calculateAndSetOffspringGenes(crossoverPoints, parentOne, parentTwo, offspringOneBitset);
        _calculateAndSetOffspringGenes(crossoverPoints, parentTwo, parentOne, offspringTwoBitset);

        _copyGenesFromBitsetToOffspring(offspringOneBitset, offspringsPair.first);
        _copyGenesFromBitsetToOffspring(offspringTwoBitset, offspringsPair.second);
    } 
}

void runMutationAlgorithm() {
    for (unsigned int i = 0; i < mutSelVec.size(); i++) { 
        float randOne = static_cast<float>(rand() % 101) / 100.0f;
        float randTwo = static_cast<float>(rand() % 101) / 100.0f;

        std::pair<bool, bool>& offspringsToMutate = get<WILL_OFFSPRINGS_BE_MUTATED_PAIR>(mutSelVec[i]);

        offspringsToMutate.first = randOne <= mutationProbability ? true : false;
        offspringsToMutate.second = randTwo <= mutationProbability ? true : false;

	    std::pair<int, int>& genesToMutateInPair = get<MUTATION_GENES_PAIR>(mutSelVec[i]);

	    if (offspringsToMutate.first) {
	        short int noOfGeneToMutate = rand() % 7; // remember 0 starts from right
	        genesToMutateInPair.first = noOfGeneToMutate;

	        Chromosome& firstSpecimenInPair = get<OFFSPRING_PAIR>(mutSelVec[i]).first;

	        switch (noOfGeneToMutate) {
	        case 0:
	    	    firstSpecimenInPair.gene0 = firstSpecimenInPair.gene0 == 0 ? 1 : 0;
	    	    break; 
	    	case 1:
	    	    firstSpecimenInPair.gene1 = firstSpecimenInPair.gene1 == 0 ? 1 : 0;
	    	    break;
	    	case 2:
	    	    firstSpecimenInPair.gene2 = firstSpecimenInPair.gene2 == 0 ? 1 : 0;
	    	    break;
	    	case 3:
	    	    firstSpecimenInPair.gene3 = firstSpecimenInPair.gene3 == 0 ? 1 : 0;
	    	    break;
	    	case 4:
	    	    firstSpecimenInPair.gene4 = firstSpecimenInPair.gene4 == 0 ? 1 : 0;
	    	    break;
	    	case 5:
	    	    firstSpecimenInPair.gene5 = firstSpecimenInPair.gene5 == 0 ? 1 : 0;
	    	    break;
	    	case 6:
	    	    firstSpecimenInPair.gene6 = firstSpecimenInPair.gene6 == 0 ? 1 : 0;
	    	    break;
	    	default:
	    	    // default will never happen
	    	    break;
	        }
	    }

	    if (offspringsToMutate.second) {
	        short int noOfGeneToMutate = rand() % 7; // remember 0 starts from right
        	genesToMutateInPair.second = noOfGeneToMutate;

	        Chromosome& secondSpecimenInPair = get<OFFSPRING_PAIR>(mutSelVec[i]).second;

	        switch (noOfGeneToMutate) {
	        case 0:
	    	    secondSpecimenInPair.gene0 = secondSpecimenInPair.gene0 == 0 ? 1 : 0;
	    	    break; 
	    	case 1:
	    	    secondSpecimenInPair.gene1 = secondSpecimenInPair.gene1 == 0 ? 1 : 0;
	    	    break;
	    	case 2:
	    	    secondSpecimenInPair.gene2 = secondSpecimenInPair.gene2 == 0 ? 1 : 0;
	    	    break;
	    	case 3:
	    	    secondSpecimenInPair.gene3 = secondSpecimenInPair.gene3 == 0 ? 1 : 0;
	    	    break;
	    	case 4:
	    	    secondSpecimenInPair.gene4 = secondSpecimenInPair.gene4 == 0 ? 1 : 0;
	    	    break;
	    	case 5:
	    	    secondSpecimenInPair.gene5 = secondSpecimenInPair.gene5 == 0 ? 1 : 0;
	    	    break;
	    	case 6:
	    	    secondSpecimenInPair.gene6 = secondSpecimenInPair.gene6 == 0 ? 1 : 0;
	    	    break;
	    	default:
	    	    // default will never happen
	    	    break;
	        }
	    }
    }
}

void copyOffspringsFromMutSelVecToChromosomeVec() {
    unsigned int chromosomeIndex = 0;

    for (unsigned int i = 0; i < mutSelVec.size(); i++) {
        std::pair<Chromosome, Chromosome>& currentOffspringPair = get<OFFSPRING_PAIR>(mutSelVec[i]);

	    // don't be afraid regarding error with two same parents. Because even if it will happen , two offsprings will be different
	    chromosomeVec[chromosomeIndex++] = currentOffspringPair.first;
	    chromosomeVec[chromosomeIndex++] = currentOffspringPair.second;
    }
}

static Chromosome& _getBestSpecimen() {
    Chromosome& highestAdaptationSpecimen = chromosomeVec[0];

    for (unsigned int i = 0; i < populationSize; i++) {
        highestAdaptationSpecimen = chromosomeVec[i].adaptation > highestAdaptationSpecimen.adaptation ? chromosomeVec[i] : highestAdaptationSpecimen;
    }

    return highestAdaptationSpecimen;
}

bool stopAlgorithmIfNoImprovementForBestSpecimenInNIterations() {
    const float adaptationForBestSpecimen = _getBestSpecimen().adaptation;

    if (adaptationForBestSpecimen == previousBestSpecimenAdaptation) {
        iterationsWithoutImprovement++;
    } else {
        iterationsWithoutImprovement = 0;
        previousBestSpecimenAdaptation  = adaptationForBestSpecimen;
    }

    return iterationsWithoutImprovement == stopAfterNIterationsWithoutImprovement ? true : false;
}

void programFinishedPrintResults() {
    cout << "\nProgram finished its run!";

    if (iterationsWithoutImprovement == stopAfterNIterationsWithoutImprovement) {
        cout << "\nAlgorithm finished because there was no change for " << iterationsWithoutImprovement << " iterations";
    }

    cout << "\nNo of generations created: " << noOfCreatedGenerations;

    const Chromosome& bestSpecimen = _getBestSpecimen();
    
    cout << "\nBest specimen's adaptation (y): " << bestSpecimen.adaptation 
         << "\nBest specimen's phenotype (x): " << bestSpecimen.phenotype << "\n\n";
}

